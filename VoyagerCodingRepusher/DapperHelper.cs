﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;

namespace VoyagerCodingRepusher
{
    public class DapperHelper
    {
        private static string _conn;
        public DapperHelper(string connString)
        {
            _conn = !string.IsNullOrEmpty(connString) ? connString : throw new ArgumentException("Connection string cannot be null or empty!");
        }

        public async Task<IList<T>> QueryAsync<T>(string sql, object parameters = null)
        {
            if (string.IsNullOrEmpty(sql))
                throw new ArgumentException("Sql query cannot be null or empty.");

            using (IDbConnection db = new SqlConnection(_conn))
            {
                var result = await db.QueryAsync<T>(sql, parameters);

                if (result != null)
                {
                    return result.ToList();
                }
            }

            return null;
        }

        public async Task<T> QuerySingleAsync<T>(string sql, object parameters = null)
        {
            if (string.IsNullOrEmpty(sql))
                throw new ArgumentException("Sql query cannot be null or empty.");

            using (IDbConnection db = new SqlConnection(_conn))
            {
                return (await db.QueryAsync<T>(sql, parameters)).FirstOrDefault();
            }
        }

        public async Task<int> ExecuteAsync(string sql, object parameters = null)
        {
            if (string.IsNullOrEmpty(sql))
                throw new ArgumentException("Sql query cannot be null or empty.");

            using (IDbConnection db = new SqlConnection(_conn))
            {
                return await db.ExecuteAsync(sql, parameters);
            }
        }

        public async Task<IEnumerable<T>> ExecuteStoredProcAsync<T>(string procName, object parameters = null)
        {
            if (string.IsNullOrEmpty(procName))
                throw new ArgumentException("Stored proc name cannot be null or empty.");

            using (IDbConnection db = new SqlConnection(_conn))
            {
                return await db.QueryAsync<T>(procName, parameters, null, null, CommandType.StoredProcedure);
            }
        }
    }
}
