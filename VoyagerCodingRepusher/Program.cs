﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using VoyagerCodingRepusher.Models;

namespace VoyagerCodingRepusher
{
    class Program
    {
        public static IConfiguration Configuration { get; set; }

        public static DapperHelper Dapper { get; set; }
        public static HttpClient Client { get; set; }

        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", true, true);

            Configuration = builder.Build();

            Client = new HttpClient();
            Client.DefaultRequestHeaders.Accept.Clear();
            Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            Dapper = new DapperHelper(Configuration.GetConnectionString("ClinicalCodingDb"));

            Task.Run(async () => await DoStuff()).Wait();

            Console.ReadLine();
        }

        public static async Task DoStuff()
        {

            var result = await Dapper.ExecuteStoredProcAsync<OutstandingCoding>("[dbo].[Voyager_GetMissingCodingDataforMMS]", 
                new { WorkflowStepTypeId = 4, DateTime.Now.Year, WorkOrderId = 141 });

            foreach (var item in result)
            {
                var request = new HttpRequestMessage(HttpMethod.Post, "http://localhost:6579/api/mms")
                {
                    Content = new StringContent(JsonConvert.SerializeObject(new {assessmentId = item.AsmtId}), Encoding.UTF8,
                        "application/json")

                };

                try
                {
                    var response = await Client.SendAsync(request, HttpCompletionOption.ResponseContentRead);
                    response.EnsureSuccessStatusCode();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }


        }


    }
}
