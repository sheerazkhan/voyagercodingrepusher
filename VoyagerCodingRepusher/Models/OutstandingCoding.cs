﻿namespace VoyagerCodingRepusher.Models
{
    public class OutstandingCoding
    {
        public int AsmtId { get; set; }
        public string WoName { get; set; }
        public string Status { get; set; }
        public string MmsId { get; set; }
    }
}
